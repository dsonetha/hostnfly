require 'test_helper'

class BookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @booking = bookings(:one)
    @booking_two = bookings(:two)
    @listing = Listing.find(@booking.listing_id)
  end

  test "should get index" do
    get listing_bookings_url(@booking), as: :json
    assert_response :success
  end

  test "should create booking" do
    assert_difference('Booking.count') do
      post listing_bookings_url(@booking), params: { booking: { end_date: @booking.end_date + 10.days, listing_id: @booking.listing_id, start_date: @booking.start_date + 10.days } }, as: :json
    end

    assert_response 201
  end

  test "should handle end_date greater than start_date booking" do
    assert_no_difference('Booking.count') do
      post listing_bookings_url(@booking), params: { booking: { end_date: @booking.start_date + 10.days, listing_id: @booking.listing_id, start_date: @booking.start_date + 10.days } }, as: :json
    end

    assert_response 422
  end

  test "should have booking overlap while creating booking" do
    assert_no_difference('Booking.count') do
      post listing_bookings_url(@booking), params: { booking: { end_date: @booking.end_date, listing_id: @booking.listing_id, start_date: @booking.start_date } }, as: :json
    end

    assert_response 409
  end

  test "should show booking" do
    get listing_booking_url(@listing, @booking), as: :json
    assert_response :success
  end

  test "should update booking" do
    patch listing_booking_url(@listing, @booking), params: { booking: { end_date: @booking.end_date + 10.days, listing_id: @booking.listing_id, start_date: @booking.start_date + 10.days } }, as: :json
    assert_response 200
  end

  test "should have booking overlap while updating booking" do
    patch listing_booking_url(@listing, @booking_two), params: { booking: { end_date: @booking.end_date, listing_id: @booking.listing_id, start_date: @booking.start_date } }, as: :json

    assert_response 409
  end

  test "should destroy booking" do
    assert_difference('Booking.count', -1) do
      delete listing_booking_url(@listing, @booking), as: :json
    end

    assert_response 204
  end
end
