class Mission < ApplicationRecord
  enum mission_type: [:first_checkin, :last_checkout, :checkout_checkin]

  belongs_to :listing

  validates_presence_of :mission_type, :date

  after_initialize :set_price

  def set_price
    listing = Listing.find(self.listing_id)
    self.price = listing.num_rooms * self.price_per_room if listing
  end

  def price_per_room
    case self.mission_type.to_sym
    when :first_checkin, :checkout_checkin
      10
    when :last_checkout
      5
    end
  end

end
