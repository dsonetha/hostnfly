require 'json'

file = File.read('../jobs/backend/backend_test.rb')
file.sub! 'input = ', ''
data_hash = JSON.parse(file)

for el in data_hash["listings"]
  Listing.create(el)
end

for el in data_hash["bookings"]
  Booking.create(el)
end

for el in data_hash["reservations"]
  Reservation.create(el)
end