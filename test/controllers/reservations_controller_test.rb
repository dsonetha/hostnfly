require 'test_helper'

class ReservationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reservation = reservations(:one)
    @listing = Listing.find(@reservation.listing_id)
  end

  test "should get index" do
    get listing_reservations_url(@reservation), as: :json
    assert_response :success
  end

  test "should create reservation" do
    assert_difference('Reservation.count') do
      post listing_reservations_url(@reservation), params: { reservation: { end_date: @reservation.end_date + 1.days, listing_id: @reservation.listing_id, start_date: @reservation.start_date + 1.days } }, as: :json
    end

    assert_response 201
  end

  test "should handle end_date greater than start_date booking" do
    assert_no_difference('Reservation.count') do
      post listing_reservations_url(@reservation), params: { reservation: { end_date: @reservation.start_date + 1.days, listing_id: @reservation.listing_id, start_date: @reservation.start_date + 1.days } }, as: :json
    end

    assert_response 422
  end

  test "should not have available booking while creating reservation" do
    assert_no_difference('Reservation.count') do
      post listing_reservations_url(@reservation), params: { reservation: { end_date: @reservation.end_date + 10.days, listing_id: @reservation.listing_id, start_date: @reservation.start_date + 10.days } }, as: :json
    end

    assert_response 400
  end

  test "should have reservation overlap while creating reservation" do
    assert_no_difference('Reservation.count') do
      post listing_reservations_url(@reservation), params: { reservation: { end_date: @reservation.end_date, listing_id: @reservation.listing_id, start_date: @reservation.start_date } }, as: :json
    end

    assert_response 409
  end

  test "should show reservation" do
    get listing_reservation_url(@listing, @reservation), as: :json
    assert_response :success
  end

  test "should update reservation" do
    patch listing_reservation_url(@listing, @reservation), params: { reservation: { end_date: @reservation.end_date + 1.days, listing_id: @reservation.listing_id, start_date: @reservation.start_date + 1.days } }, as: :json
    assert_response 200
  end

  test "should not have available booking while updating reservation" do
    patch listing_reservation_url(@listing, @reservation), params: { reservation: { end_date: @reservation.end_date + 10.days, listing_id: @reservation.listing_id, start_date: @reservation.start_date + 10.days } }, as: :json
    assert_response 400
  end

  test "should have reservation overlap while updating reservation" do
    patch listing_reservation_url(@listing, reservations(:two)), params: { reservation: { end_date: @reservation.end_date, listing_id: @reservation.listing_id, start_date: @reservation.start_date } }, as: :json
    assert_response 409
  end

  test "should destroy reservation" do
    assert_difference('Reservation.count', -1) do
      delete listing_reservation_url(@listing, @reservation), as: :json
    end

    assert_response 204
  end
end
