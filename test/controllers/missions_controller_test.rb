require 'test_helper'

class MissionControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get missions_url, as: :json
    assert_response :success
  end
end
