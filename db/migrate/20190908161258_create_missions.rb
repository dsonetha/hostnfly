class CreateMissions < ActiveRecord::Migration[5.1]
  def change
    create_table :missions do |t|
      t.date :date
      t.integer :mission_type
      t.integer :price
      t.belongs_to :listing, foreign_key: true

      t.timestamps
    end
  end
end
