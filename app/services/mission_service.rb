class MissionService
  attr_reader :mission_list

  def initialize
    listing_list = Listing.all
    @mission_list = []

    for listing in listing_list
      # no cleaning missions if no reservations
      next if listing.reservations.empty?

      reservation_list = listing.reservations.order(:start_date)

      # for a booking
      for booking in listing.bookings.order(:start_date)
      	# init all reservations in the given booking
      	booking_reservations = reservation_list.select{ |item| item.start_date >= booking.start_date && item.end_date <= booking.end_date }

      	last_index = booking_reservations.size - 1
      	booking_reservations.each_with_index do |reservation, i|
      	  # add a checkin_checkout mission if not the last reservation
          @mission_list << listing.missions.new({:date => reservation.end_date, :mission_type => :checkout_checkin}) if i != last_index
      	end

      	# add first_checkin / last_checkout if needed
      	if !booking_reservations.empty?
          @mission_list << listing.missions.new({:date => booking.start_date, :mission_type => :first_checkin})
          @mission_list << listing.missions.new({:date => booking.end_date, :mission_type => :last_checkout})
        end
      end

    end
  end
end
