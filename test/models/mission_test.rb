require 'test_helper'

class MissionTest < ActiveSupport::TestCase
  setup do
    @listing = listings(:one)
  end

  test "set price" do
    mission = @listing.missions.new mission_type: :first_checkin, date: "2019-09-03"
    assert mission.price == 20

    mission_two = @listing.missions.new mission_type: :last_checkout, date: "2019-09-03"
    assert mission_two.price == 10
  end
end
