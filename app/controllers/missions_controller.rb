class MissionsController < ApplicationController
  def index
    @missions = MissionService.new().mission_list
    render json: @missions
  end
end
