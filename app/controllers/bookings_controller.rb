class BookingsController < ApplicationController
  before_action :set_listing
  before_action :set_booking, only: [:show, :update, :destroy]

  # GET /listings/:listing_id/bookings
  def index
    @bookings = @listing ? @listing.bookings : []

    render json: @bookings
  end

  # GET /listings/:listing_id/bookings/1
  def show
    render json: @booking
  end

  # POST /listings/:listing_id/bookings
  def create
    @booking = @listing.bookings.new(booking_params)

    if has_overlap
      render json: "Booking dates overlap", status: :conflict
    elsif @booking.save
      render json: @booking, status: :created, location: @listing_booking
    else
      render json: @booking.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /listings/:listing_id/bookings/1
  def update
    @booking.assign_attributes(booking_params)
    if has_overlap
      render json: "Booking dates overlap", status: :conflict
    elsif @booking.save
      render json: @booking
    else
      render json: @booking.errors, status: :unprocessable_entity
    end
  end

  # DELETE /listings/:listing_id/bookings/1
  def destroy
    @booking.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    def set_listing
      @listing = Listing.find(params[:listing_id])
    end

    # Only allow a trusted parameter "white list" through.
    def booking_params
      params.require(:booking).permit(:start_date, :end_date)
    end

    # Check if two bookings has date overlap
    def has_overlap
      if @listing && @booking
        !!@listing.bookings.find {|b|
          next if b.id == @booking.id
          (b.start_date >= @booking.start_date && b.start_date < @booking.end_date) ||
          (b.end_date > @booking.start_date && b.end_date <= @booking.end_date)
        }
      else
        false
      end
    end
end
