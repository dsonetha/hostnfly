Rails.application.routes.draw do
  resources :missions, only: [:index]
  resources :listings do
    resources :reservations
    resources :bookings
  end
end
