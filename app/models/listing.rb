class Listing < ApplicationRecord
  has_many :bookings, dependent: :destroy
  has_many :reservations, dependent: :destroy
  has_many :missions, dependent: :destroy

  validates_presence_of :num_rooms
  validates_numericality_of :num_rooms, :greater_than => 0
end
