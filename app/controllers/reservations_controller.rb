class ReservationsController < ApplicationController
  before_action :set_listing
  before_action :set_reservation, only: [:show, :update, :destroy]

  # GET /listings/:listing_id/reservations
  def index
    @reservations = @listing ? @listing.reservations : []

    render json: @reservations
  end

  # GET /listings/:listing_id/reservations/1
  def show
    render json: @reservation
  end

  # POST /listings/:listing_id/reservations
  def create
    @reservation = @listing.reservations.new(reservation_params)

    if has_overlap
      render json: "Reservation dates overlap", status: :conflict
    elsif !has_an_available_booking
      render json: "No available booking for these dates", status: :bad_request
    elsif @reservation.save
      render json: @reservation, status: :created, location: @listing_reservation
    else
      render json: @reservation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /listings/:listing_id/reservations/1
  def update
    @reservation.assign_attributes(reservation_params)
    if has_overlap
      render json: "Reservation dates overlap", status: :conflict
    elsif !has_an_available_booking
      render json: "No available booking for these dates", status: :bad_request
    elsif @reservation.save
      render json: @reservation
    else
      render json: @reservation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /listings/:listing_id/reservations/1
  def destroy
    @reservation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    def set_listing
      @listing = Listing.find(params[:listing_id])
    end

    # Only allow a trusted parameter "white list" through.
    def reservation_params
      params.require(:reservation).permit(:start_date, :end_date)
    end

    # Check if two reservations has date overlap
    def has_overlap
      if @listing && @reservation
        !!@listing.reservations.find {|r|
          next if r.id == @reservation.id
          (r.start_date >= @reservation.start_date && r.start_date < @reservation.end_date) ||
          (r.end_date > @reservation.start_date && r.end_date <= @reservation.end_date)
        }
      else
        false
      end
    end

    # Check if a booking has available dates for the reservation
    def has_an_available_booking
      if @listing && @reservation
        !!@listing.bookings.find {|b| b.start_date <= @reservation.start_date && b.end_date >= @reservation.end_date }
      else
        false
      end
    end
end
